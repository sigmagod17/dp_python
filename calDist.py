import MySQLdb;
import requests;
import json;

dsn_database = "db_name";
dsn_hostname = "server_ip";
dsn_port = dbport;
dsn_uid = "db_user_account";
dsn_pwd = "db_user_passwowrd";

conn = MySQLdb.connect(host=dsn_hostname, port=dsn_port, user=dsn_uid, passwd=dsn_pwd, db=dsn_database, charset='utf8');

placeDict = dict();
distDict = dict();

searchUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json";
searchKey = "YOUR_API_KEY_FOR_TEXTSEARCH";
calDistUrl = "https://maps.googleapis.com/maps/api/distancematrix/json";
calDistKey = "YOUR_API_KEY_FOR_DISTANCEMATRIX";

sql = "SELECT EMPLOYEE_ID,AREA_NO,JOB_AREA FROM dm_final.EM_PNL";
cursor.execute(sql);
result = cursor.fetchall();

for row in result:
    empId = row[0];
    originCode = row[1];
    destCode = row[2];
    
    if (originCode == '000'):
        continue;
        
    #Get origin address
    print 'start query ' + originCode;
    if placeDict.has_key(originCode):
        originPos = placeDict[originCode];
    else:
        param = {'query': "TW " + originCode, 'language': 'zh-TW', 'key': searchKey}
        response = requests.get(searchUrl, params=param);
        repJson = response.json();
        originPos = repJson['results'][0]['formatted_address'];
        placeDict[originCode] = originPos;
    
    #Get destination address
    if placeDict.has_key(destCode):
        destPos = placeDict[destCode];
    else:
        param = {'query': "TW " + destCode, 'language': 'zh-TW', 'key': searchKey}
        response = requests.get(searchUrl, params=param);
        repJson = response.json();
        destPos = repJson['results'][0]['formatted_address'];
        placeDict[destCode] = destPos;

    #Calculate distance between origin and destination
    dictKey = originCode + "," + destCode;
    if distDict.has_key(dictKey):
        distToKM = distDict[dictKey];
    else:               
        param = {'mode': 'driving', 'origins': originPos, 'destinations': destPos, 'key': calDistKey};
        response = requests.get(calDistUrl, params=param);
        repJson = response.json();
        dist = repJson['rows'][0]['elements'][0]['distance']['value'];
        distToKM = round((dist/1000.0),1);
        distDict[dictKey] = distToKM;

    print originPos + " to " + destPos + " = " + str(distToKM);
    cursor.execute("UPDATE dm_final.EM_PNL SET JOB_DIST=%s WHERE EMPLOYEE_ID=%s",(distToKM,row[0]));

conn.commit();
conn.close();
del placeDict;
del distDict;
