import MySQLdb;
from dateutil import parser;
from dateutil import relativedelta as rdelta;

dsn_database = "db_name";
dsn_hostname = "server_ip";
dsn_port = dbport;
dsn_uid = "db_user_account";
dsn_pwd = "db_user_passwowrd";

conn = MySQLdb.connect(host=dsn_hostname, port=dsn_port, user=dsn_uid, passwd=dsn_pwd, db=dsn_database, charset='utf8');

sql = "SELECT EMPLOYEE_ID,HIRE_D,LEAVE_DT FROM dm_final.EM_PNL";
cursor.execute(sql);
result = cursor.fetchall();
cald = parser.parse("20170531");
for row in result:
    hiredStr = row[1];
    hired = parser.parse(hiredStr);
    
    #consider about the employee leaved
    if (row[2] is not None):
        cald = parser.parse(row[2]);
        
    duration = rdelta.relativedelta(cald,hired);
    yoe = duration.years + round((duration.months/12.0),1);
    print str(row[0]) + ": " + str(yoe);
    cursor.execute("UPDATE dm_final.EM_PNL SET JOB_YEAR=%s WHERE EMPLOYEE_ID=%s",(yoe,row[0]));
    
conn.commit();
conn.close();